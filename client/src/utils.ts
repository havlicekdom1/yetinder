import { Gender } from "./types"

export const parseGender = (gender: Gender) => {
  switch (gender) {
    case 'male':
      return 'yeti'

    case 'female':
      return 'yetinka'
  }
}