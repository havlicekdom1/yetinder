import { Yeti } from '../types';
import apiClient from './client';

type NewYetiDto = Omit<Yeti, 'id' | 'likes'>;

export enum RankType {
  like = 'like',
  dislike = 'dislike',
}

type RankYetiDto = {
  type: RankType
}

export const getBestOf = async(): Promise<Yeti[]> => (await apiClient.get('/yeti/best-of')).data

export const getMatch = async(): Promise<Yeti> => (await apiClient.get('/yeti/match')).data

export const createNewYeti = async(body: NewYetiDto): Promise<void> => await apiClient.post('/yeti/', body)

export const rankYeti = async({ id, body } : { id: string, body: RankYetiDto }): Promise<void> => await apiClient.post(`/yeti/rank/${id}`, body)