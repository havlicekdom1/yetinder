import axios from "axios"
import { apiUrl } from "../constants"

export default axios.create({
  baseURL: apiUrl,
})