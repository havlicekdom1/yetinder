import { Statistic } from '../types';
import apiClient from './client';


export const getStatistics = async(): Promise<Statistic[]> => (await apiClient.get('/statistics/')).data
