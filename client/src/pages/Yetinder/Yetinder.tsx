import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import React from 'react'
import { getMatch, rankYeti, RankType } from '../../api/yeti'
import PageTitle from '../../components/PageTitle'

import styles from './Yetinder.module.css'

function Yetinder() {
  const query = useQuery({
    queryKey: ['matchYeti'],
    queryFn: getMatch
  })

  const queryClient = useQueryClient()

  const mutation = useMutation({
    mutationFn: rankYeti,
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ['matchYeti'] })
    },
  })

  const handleLike = () => {
    mutation.mutate({
      id: query.data?.id ?? '',
      body: {
        type: RankType.like
      }
    })
  }

  const handleDislike = () => {
    mutation.mutate({
      id: query.data?.id ?? '',
      body: {
        type: RankType.dislike
      }
    })
  }

  return (
    <div className={styles.yetiWrapper}>
      <div className={styles.yeti}>
        <div className={styles.yetiName}>
          { query.data?.name }
        </div>
        <div className={styles.yetiLocation}>
          { query.data?.location }
        </div>
        <div className={styles.yetiAge}>
          { query.data?.age } let
        </div>
        <div className={styles.yetiHeight}>
          { query.data?.height } cm
        </div>
        <div className={styles.yetiWeigth}>
          { query.data?.weigth } kg
        </div>
      </div>
      <div className={styles.buttonGroup}>
        <button className={styles.button} onClick={handleLike}>
          +1
        </button>
        <button className={styles.button} onClick={handleDislike}>
          -1
        </button>
      </div>
    </div>
  )
}

export default Yetinder