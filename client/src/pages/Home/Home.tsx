import React from 'react'
import { useQuery } from '@tanstack/react-query'
import { getBestOf } from '../../api/yeti'
import PageTitle from '../../components/PageTitle'
import { parseGender } from '../../utils'

import styles from './Home.module.css'

function Home() {
  const query = useQuery({ queryKey: ['yetis'], queryFn: getBestOf })
  
  return (
    <div className="w-full">
      <PageTitle>
        Best of yeti
      </PageTitle>
      <table className={styles.table}>
        <thead className={styles.tableHead}>
          <tr>
            <th className={styles.tableHeadCell}>Pořadí</th>
            <th className={styles.tableHeadCell}>Jméno</th>
            <th className={styles.tableHeadCell}>Věk</th>
            <th className={styles.tableHeadCell}>Výška</th>
            <th className={styles.tableHeadCell}>Váha</th>
            <th className={styles.tableHeadCell}>Bydliště</th>
            <th className={styles.tableHeadCell}>Pohlaví</th>
          </tr>
        </thead>
        <tbody>
          {query.data?.map((yeti, index) => (
            <tr key={yeti.id} className={styles.tableRow}>
              <td className={styles.tableCell}>{ index + 1 }</td>
              <td className={styles.tableCell}>{ yeti.name }</td>
              <td className={styles.tableCell}>{ yeti.age } let</td>
              <td className={styles.tableCell}>{ yeti.height } cm</td>
              <td className={styles.tableCell}>{ yeti.weigth } kg</td>
              <td className={styles.tableCell}>{ yeti.location }</td>
              <td className={styles.tableCell}>{ parseGender(yeti.gender) }</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default Home