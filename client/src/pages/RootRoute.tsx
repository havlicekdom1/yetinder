import React from 'react'
import { Outlet } from 'react-router'
import Header from '../components/Header/Header'

function RootRoute() {
  return (
    <div className="min-h-screen bg-white dark:bg-slate-800 dark:text-slate-200">
      <Header />
      <main className="p-6 mt-4 w-full flex">
        <Outlet />
      </main>
    </div>
  )
}

export default RootRoute