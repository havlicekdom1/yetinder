import { useQuery } from '@tanstack/react-query'
import React from 'react'
import { getStatistics } from '../../api/statistics'
import PageTitle from '../../components/PageTitle';
import { parseGender } from '../../utils';

import styles from './Statistics.module.css';

function Statistics() {
  const query = useQuery({
    queryKey: ['statistics'],
    queryFn: getStatistics,
  })

  return (
    <div className="w-full">
      <PageTitle>
        Statistiky
      </PageTitle>
      <table className={styles.table}>
        <thead className={styles.tableHead}>
          <tr>
            <th className={styles.tableHeadCell}>Jméno</th>
            <th className={styles.tableHeadCell}>Věk</th>
            <th className={styles.tableHeadCell}>Výška</th>
            <th className={styles.tableHeadCell}>Váha</th>
            <th className={styles.tableHeadCell}>Bydliště</th>
            <th className={styles.tableHeadCell}>Pohlaví</th>
            <th className={styles.tableHeadCell}>Liků dnes</th>
            <th className={styles.tableHeadCell}>Liků za poslední měsíc</th>
            <th className={styles.tableHeadCell}>Liků za poslední rok</th>
          </tr>
        </thead>
        <tbody>
          {query.data?.map((statistic) => (
            <tr key={statistic.name} className={styles.tableRow}>
              <td className={styles.tableCell}>{ statistic.name }</td>
              <td className={styles.tableCell}>{ statistic.age } let</td>
              <td className={styles.tableCell}>{ statistic.height } cm</td>
              <td className={styles.tableCell}>{ statistic.weigth } kg</td>
              <td className={styles.tableCell}>{ statistic.location }</td>
              <td className={styles.tableCell}>{ parseGender(statistic.gender) }</td>
              <td className={styles.tableCell}>{ statistic.dailyLikes ?? 0 }</td>
              <td className={styles.tableCell}>{ statistic.monthlyLikes ?? 0 }</td>
              <td className={styles.tableCell}>{ statistic.yearlyLikes ?? 0 }</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default Statistics