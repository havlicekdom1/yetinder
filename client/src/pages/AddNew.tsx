import React from 'react'
import AddYetiForm from '../components/AddYetiForm/AddYetiForm'
import PageTitle from '../components/PageTitle'

function AddNew() {
  return (
    <div>
      <PageTitle>
        Přidat nového yetiho
      </PageTitle>
      <AddYetiForm />
    </div>
  )
}

export default AddNew