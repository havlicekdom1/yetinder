import { Route, Routes } from "react-router"
import Home from "./pages/Home/Home"
import RootRoute from "./pages/RootRoute"
import AddNew from './pages/AddNew'
import Yetinder from "./pages/Yetinder/Yetinder"
import Statistics from "./pages/Statistics/Statistics"

function App() {
  return (
    <Routes>
      <Route path="/" element={<RootRoute />}>
        <Route path="/" element={<Home />} />
        <Route path="/add-new" element={<AddNew />} />
        <Route path="/yetinder" element={<Yetinder />} />
        <Route path="/statistics" element={<Statistics />} />
      </Route>
    </Routes>
  )
}

export default App
