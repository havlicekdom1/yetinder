import React from 'react'

type Props = {
  children: React.ReactNode
}

function PageTitle({ children }: Props) {
  return (
    <h1 className="font-bold text-4xl mb-4">
      { children }
    </h1>
  )
}

export default PageTitle