import React from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { Gender, Yeti } from '../../types'

import styles from './AddYetiForm.module.css'
import { parseGender } from '../../utils'
import classnames from 'classnames'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import { createNewYeti } from '../../api/yeti'

type FormValues = Omit<Yeti, 'id' | 'likes'>

const schema = yup.object({
  name: yup.string().required('Jméno je povinné pole'),
  age: yup.number().required('Věk je povinné pole'),
  height: yup.number().required('Výška je povinné pole'),
  weigth: yup.number().required('Váha je povinné pole'),
  location: yup.string().required('Bydliště je povinné pole'),
  gender: yup.mixed<Gender>().oneOf(Object.values(Gender)).required(),
}).required()

function AddYetiForm() {
  const queryClient = useQueryClient()

  const mutation = useMutation({
    mutationFn: createNewYeti,
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: ['yetis'] })
    },
  })
  
  const { register, handleSubmit, formState: { errors }, reset } = useForm<FormValues>({
    resolver: yupResolver(schema),
  })

  const onSubmit = (data: FormValues) => {
    mutation.mutate(data)
    reset()
  }

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)} noValidate>
      <input className={classnames(styles.input, { [styles.inputError]: !!errors.name })} placeholder="Jméno" type="text" {...register('name')} />
      { errors.name && (<span className={styles.inputErrorMessage}>{ errors.name.message }</span>)}
      <input className={classnames(styles.input, { [styles.inputError]: !!errors.age })} placeholder="Věk" type="number" {...register('age')} />
      { errors.age && (<span className={styles.inputErrorMessage}>{ errors.age.message }</span>)}
      <input className={classnames(styles.input, { [styles.inputError]: !!errors.height })} placeholder="Výška" type="number" {...register('height')} />
      { errors.height && (<span className={styles.inputErrorMessage}>{ errors.height.message }</span>)}
      <input className={classnames(styles.input, { [styles.inputError]: !!errors.weigth })} placeholder="Váha" type="number" {...register('weigth')} />
      { errors.weigth && (<span className={styles.inputErrorMessage}>{ errors.weigth.message }</span>)}
      <input className={classnames(styles.input, { [styles.inputError]: !!errors.location })} placeholder="Bydliště" type="text" {...register('location')} />
      { errors.location && (<span className={styles.inputErrorMessage}>{ errors.location.message }</span>)}
      <div className={styles.radioGroup}>
        { Object.values(Gender).map((gender, index) => (
          <div key={gender}>
            <input className={styles.radio} type="radio" {...register('gender')} id={`radio-${gender}`} value={gender} defaultChecked={index === 0} />
            <label htmlFor={`radio-${gender}`}>{ parseGender(gender) }</label>
          </div>
        )) }
      </div>
      <button className={styles.button} type="submit">
        Přidat
      </button>
    </form>
  )
}

export default AddYetiForm