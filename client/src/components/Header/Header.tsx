import { Link, NavLink } from "react-router-dom"
import classnames from 'classnames'

import styles from './Header.module.css'

const getNavLinkClasses = (isActive: boolean) => classnames(styles.menuLink, { [styles.menuLinkActive]: isActive })

function Header() {
  return (
    <header>
      <nav className={styles.headerNav}>
        <div className={styles.headerDiv}>
          <Link to="/" className="flex items-center">
            <span className={styles.headerBrand}>Yetinder</span>
          </Link>
          <div className={styles.headerMenuWrapper}>
            <ul className={styles.headerMenu}>
              <li>
                <NavLink to="/" className={({ isActive } : { isActive: boolean }) => getNavLinkClasses(isActive)}>
                  Domů
                </NavLink>
              </li>
              <li>
                <NavLink to="/add-new" className={({ isActive } : { isActive: boolean }) => getNavLinkClasses(isActive)}>
                  Přidat yetiho
                </NavLink>
              </li>
              <li>
                <NavLink to="/yetinder" className={({ isActive } : { isActive: boolean }) => getNavLinkClasses(isActive)}>
                  Yetinder
                </NavLink>
              </li>
              <li>
                <NavLink to="/statistics" className={({ isActive } : { isActive: boolean }) => getNavLinkClasses(isActive)}>
                  Statistiky
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  )
}

export default Header