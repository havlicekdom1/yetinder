export enum Gender {
  male = 'male',
  female = 'female',
}

export type Yeti = {
  id: string;
  name: string;
  age: number;
  height: number;
  weigth: number;
  location: string;
  gender: Gender;
  likes: number;
}

export type Statistic = Omit<Yeti, "id" | "likes"> | {
  yearlyLikes: number;
  monthlyLikes: number;
  dailyLikes: number;
}