# Yetinder

Projekt Yetinder pro WPJ

## Technologie

### Server
* PHP
* MySQL
* Symfony

### Klient
* React
* Typescript
* Vite

## Jak co spustit

### Server
Server je postavený nad Symfony, ke spuštění je potřeba udělat následující:

```
composer install
```

nainstaluje veškeré závislosti.

```
symfony server:start
```

potom zapne PHP server a začne poslouchat na portu 8000.

### Klient
Postup klasický pro React aplikace. Prvně nainstalujeme závislosti pomocí příkazu
```
yarn
```

potom pomocí
```
yarn dev
```

spustíme aplikaci. Tu pak můžeme vidět na `http://localhost:5173/`.