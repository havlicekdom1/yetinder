<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

class ErrorController extends AbstractController
{
    public function show(FlattenException $exception, LoggerInterface $logger): Response
    {
        $logger->debug($exception->getAsString());

        return new Response($exception->getMessage(), $exception->getStatusCode());
    }
}
