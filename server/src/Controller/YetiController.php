<?php

namespace App\Controller;

use App\Service\YetiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/yeti')]
class YetiController extends AbstractController
{
    #[Route('/', name: 'get_all_yetis', methods: ['GET'])]
    /**
     * Get all handler
     *
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function getAll(
        YetiService $yetiService,
    ): JsonResponse {
        $yetis = $yetiService->findAll();

        return $this->json($yetis);
    }

    #[Route('/', name: 'create_new_yeti', methods: ['POST'])]
    /**
     * Create new handler
     *
     * @param Request $request
     * @param YetiService $yetiService
     * @return Response
     */
    public function create(
        Request $request,
        YetiService $yetiService,
    ): Response {
        $yetiService->create(json_decode($request->getContent()));

        return new Response('Created.', Response::HTTP_CREATED);
    }

    #[Route('/best-of', name: 'best_of_yeti', methods: ['GET'])]
    /**
     * Best of handler
     *
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function bestOf(
        YetiService $yetiService,
    ): JsonResponse {
        $yetis = $yetiService->getBestOf();

        return $this->json($yetis);
    }

    #[Route('/match', name: 'match_yeti', methods: ['GET'])]
    /**
     * Get match handler
     *
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function getMatch(
        YetiService $yetiService,
    ): JsonResponse {
        $yeti = $yetiService->getMatch();

        return $this->json($yeti);
    }

    #[Route('/rank/{id}', name: 'rank_yeti', methods: ['POST'])]
    /**
     * Rank yeti handler
     *
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function rankYeti(
		string $id,
		Request $request,
        YetiService $yetiService,
    ): Response {
        $yetiService->rankYeti($id, json_decode($request->getContent()));

        return new Response();
    }

    #[Route('/{id}', name: 'get_one_yeti', methods: ['GET'])]
    /**
     * Get one handler
     *
     * @param string $id
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function getOne(
        string $id,
        YetiService $yetiService,
    ): JsonResponse {
        $yeti = $yetiService->find($id);

        return $this->json($yeti);
    }

    #[Route('/{id}', name: 'update_one_yeti', methods: ['PUT'])]
    /**
     * Update one handler
     *
     * @param string $id
     * @param Request $request
     * @param YetiService $yetiService
     * @return JsonResponse
     */
    public function update(
        string $id,
        Request $request,
        YetiService $yetiService,
    ): JsonResponse {
        return $this->json(
            $yetiService->update($id, json_decode($request->getContent()))
        );
    }

    #[Route('/{id}', name: 'delete_one_yeti', methods: ['DELETE'])]
    /**
     * Delete handler
     *
     * @param string $id
     * @param YetiService $yetiService
     * @return Response
     */
    public function delete(
        string $id,
        YetiService $yetiService,
    ): Response {
        $yetiService->delete($id);
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
