<?php

namespace App\Controller;

use App\Service\StatisticsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/statistics')]
class StatisticsController extends AbstractController
{
    #[Route('/', name: 'get_all_stats', methods: ['GET'])]
	/**
	 * Get all statistics handler
	 * 
	 * @param StatisticsService $statisticsService
	 * @return JsonResponse
	 */
    public function getAll(
		StatisticsService $statisticsService,
	): JsonResponse
    {
		$stats = $statisticsService->getStatistics();

        return $this->json($stats);
    }
}
