<?php

namespace App\Service;

use App\Repository\StatisticRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Statistics Service
 */
class StatisticsService implements ServiceEntityRepositoryInterface {
    private StatisticRepository $statisticRepository;

    /**
	 * @param StatisticRepository $statisticRepository
	 */
    public function __construct(
        StatisticRepository $statisticRepository,
    ) {
        $this->statisticRepository = $statisticRepository;
    }

	/**
	 * Return statistics for all yetis
	 *
	 * @return array
	 */
	public function getStatistics(): array {
		return $this->statisticRepository->countStatistics();
	}
}