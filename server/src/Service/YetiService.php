<?php

namespace App\Service;

use App\Entity\Statistic;
use App\Entity\Yeti;
use App\Repository\StatisticRepository;
use App\Repository\YetiRepository;
use App\Enum\RankType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Yeti Service
 */
class YetiService implements ServiceEntityRepositoryInterface {
    private const BEST_OF_LIMIT = 10;
    private YetiRepository $yetiRepository;
    private StatisticRepository $statisticRepository;
    private ValidatorInterface $validator;
    private LoggerInterface $logger;

    /**
     * @param YetiRepository $yetiRepository
     * @param ValidatorInterface $validator
     * @param LoggerInterface $logger
     */
    public function __construct(
        YetiRepository $yetiRepository,
        StatisticRepository $statisticRepository,
        ValidatorInterface $validator,
        LoggerInterface $logger,
    ) {
        $this->yetiRepository = $yetiRepository;
        $this->statisticRepository = $statisticRepository;
        $this->validator = $validator;
        $this->logger = $logger;
    }
    /**
     * Returns all yetis in database
     *
     * @return array
     */
    public function findAll(): array {
        return $this->yetiRepository->findAll();
    }

    /**
     * Returns yeti with specified ID
     *
     * @param string $id
     * @throws NotFoundHttpException
     * @return Yeti
     */
    public function find(string $id): Yeti {
        $yeti = $this->yetiRepository->find($id);

        if (!$yeti) throw new NotFoundHttpException('Entity not found');

        return $yeti;
    }

    /**
     * Creates new yeti from post body data
     *
     * @param object $data
     * @return void
     */
    public function create($data) {
        $yeti = new Yeti();
        $this->setYetiAttributes($yeti, $data);
        $this->validateYeti($yeti);
        $this->yetiRepository->save($yeti, true);
    }

    /**
     * Updates yeti with specified ID and post data
     *
     * @param string $id
     * @param object $data
     * @return void
     */
    public function update(string $id, $data) {
        $yetiToUpdate = $this->find($id);

        if (!$yetiToUpdate) throw new NotFoundHttpException('Entity not found');

        $this->setYetiAttributes($yetiToUpdate, $data);
        $this->yetiRepository->save($yetiToUpdate, true);
    }

    /**
     * Deletes yeti from database based on ID
     *
     * @param string $id
     * @return void
     */
    public function delete(string $id) {
        $yetiToDelete = $this->find($id);

        if (!$yetiToDelete) throw new NotFoundHttpException('Entity not found');

        $this->yetiRepository->remove($yetiToDelete, true);
    }

    /**
     * Gets top 10 yetis
     *
     * @return array
     */
    public function getBestOf(): array {
        return $this->yetiRepository->findBy(array(), array('likes' => 'DESC'), $this::BEST_OF_LIMIT);
    }

    /**
     * Gets Yeti for match
     *
     * @return Yeti
     */
    public function getMatch(): Yeti {
        $yetis = $this->findAll();

        if (!$yetis) throw new NotFoundHttpException('No yetis to match');

        $randomIndex = round(rand(0, count($yetis) - 1));
        $this->logger->debug($randomIndex);

        return $yetis[$randomIndex];
    }

	/**
	 * Rank yeti based on ID
	 *
	 * @param string $id
	 * @param object $data
	 * @return void
	 */
	public function rankYeti(string $id, $data) {
		$yeti = $this->find($id);
		$this->setYetiLikesAndLogStatistic($yeti, RankType::from($data->type));
	}

    /**
     * Sets the attributes of provided yeti from post data
     *
     * @param Yeti $yeti
     * @param object $data
     * @return void
     */
    private function setYetiAttributes(Yeti $yeti, $data) {
        $yeti->setName($data->name);
        $yeti->setAge($data->age);
        $yeti->setHeight($data->height);
        $yeti->setWeigth($data->weigth);
        $yeti->setLocation($data->location);
        $yeti->setGender($data->gender);
    }

    /**
     * Validates yeti object
     *
     * @param Yeti $yeti
     * @return void
     */
    private function validateYeti(Yeti $yeti) {
        $errors = $this->validator->validate($yeti);

        $this->logger->error($errors);

        if (count($errors) > 0) {
            throw new UnprocessableEntityHttpException('Invalid data');
        }
    }

	/**
	 * Updates likes of yeti based on type of ranking and logs the state to statistics
	 *
	 * @param Yeti $yeti
	 * @param RankEnum $type
	 * @return void
	 */
	private function setYetiLikesAndLogStatistic(
		Yeti $yeti,
		RankType $type,
	) {
		switch ($type) {
			case RankType::Like:
				$yeti->setLikes($yeti->getLikes() + 1);
				break;

			case RankType::Dislike:
				$yeti->setLikes($yeti->getLikes() - 1);
				break;
			
			default:
				break;
		}

		$this->createLikesStatistic($yeti);
		$this->yetiRepository->save($yeti, true);
	}

	/**
	 * Creates new statistic record for yeti
	 *
	 * @param Yeti $yeti
	 * @return void
	 */
	private function createLikesStatistic(Yeti $yeti) {
		$statistic = new Statistic();
		$statistic->setYeti($yeti);
		$statistic->setLikes($yeti->getLikes());
		$statistic->setCreated(new \DateTime());

		$this->statisticRepository->save($statistic, true);
	}
}