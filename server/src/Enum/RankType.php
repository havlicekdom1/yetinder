<?php

namespace App\Enum;

enum RankType: string {
	case Like = 'like';
	case Dislike = 'dislike';
}