<?php

namespace App\Repository;

use App\Entity\Statistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Statistic>
 *
 * @method Statistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistic[]    findAll()
 * @method Statistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Statistic::class);
    }

    public function save(Statistic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Statistic $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
	/**
	 * Queries the DB and counts all the different statistics for yetis
	 *
	 * @return array
	 */
	public function countStatistics(): array {
		$dbQuery = "SELECT SUM(CASE WHEN s.created BETWEEN DATE_SUB(NOW(), INTERVAL 1 DAY) AND NOW() THEN s.likes END) AS dailyLikes,
		SUM(CASE WHEN s.created BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW() THEN s.likes END) AS monthlyLikes,
		SUM(CASE WHEN s.created BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) AND NOW() THEN s.likes END) AS yearlyLikes,
		y.name, y.age, y.height, y.weigth, y.location, y.gender
		FROM statistic AS s
		JOIN yeti AS y
		ON s.yeti_id = y.id
		GROUP BY s.yeti_id";

		return $this->getEntityManager()
			->getConnection()
			->prepare($dbQuery)
			->executeQuery()
			->fetchAllAssociative();
	}
}
